---
title: InappbrowserEx
description: Open an in-app browser window.
---

# cordova-plugin-inappbrowser-ex

A customized version of the Cordova plugin 'cordova-plugin-inappbrowser'.
