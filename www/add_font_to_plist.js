var fs = require('fs');
var path = require('path');
var plist = require('plist');

module.exports = function(context) {

    const ConfigParser = require('cordova-common').ConfigParser;
    const config = new ConfigParser('config.xml');
    const appName = config.name();

    var platformPath = path.join(context.opts.projectRoot, 'platforms/ios');
    var infoPlistPath = path.join(platformPath, appName, appName+'-Info.plist');

    var infoPlist = plist.parse(fs.readFileSync(infoPlistPath, 'utf8'));

    if (!infoPlist.UIAppFonts) {
        infoPlist.UIAppFonts = [];
    }

    // add the font file names in the following array 
    var fontNames = ['DubaiW23-Regular.ttf','DubaiW23-Bold.ttf'];

    fontNames.forEach(function(fontName) {
        if (!infoPlist.UIAppFonts.includes(fontName)) {
            infoPlist.UIAppFonts.push(fontName);
        }
    });

    fs.writeFileSync(infoPlistPath, plist.build(infoPlist));
};